package com.account.book.dao;

import org.junit.jupiter.api.Test;

import java.util.Calendar;
import java.util.Date;

/**
 * @author youlingdada youlingdada@163.com
 * @version 1.0
 * @createDate 2021/11/10 21:09
 */
public class Demo {
    @Test
    void test() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2021, 10, 29);
        System.out.println(calendar.get(Calendar.DATE));
        int i = calendar.get(Calendar.DAY_OF_WEEK);
        if (i == 1) {
            System.out.println("before:" + (28 - (7 - i)));
            System.out.println("after:" + 28);
        } else {
            System.out.println(29 - i + 2);
            System.out.println(29 + 7 - i);
        }
        System.out.println(i);
    }


    @Test
    void getWeekStartAndEnd() {

        int year, mouth, date;
        year = 2021;
        mouth = 10;
        date = 30;
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, mouth, date, 23, 59, 59);
        Date beforeDate, afterDate;
        afterDate = calendar.getTime();
        int weekDay = calendar.get(Calendar.DAY_OF_WEEK);
        if (weekDay == 1) { // 当前日期为星期天
            calendar.add(Calendar.DATE, -6);
        } else {
            calendar.add(Calendar.DATE, -weekDay + 2);
        }
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        beforeDate = calendar.getTime();

    }

    @Test
    void demo() {

    }
}

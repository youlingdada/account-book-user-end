package com.account.book.dao;

import com.account.book.entity.Icon;
import com.account.book.entity.IconEntity;
import com.account.book.service.IconEntityService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @author youlingdada youlingdada@163.com
 * @version 1.0
 * @createDate 2021/11/7 10:19
 */
@SpringBootTest
public class IconEntityDaoTest {

    @Autowired
    private IconEntityDao iconEntityDao;

    @Test
    void add() {
        IconEntity iconEntity = new IconEntity();
        iconEntity.setIconTypeId(1456971922954809346L);
        iconEntity.setUrl("test url3");
        iconEntityDao.insert(iconEntity);
        IconEntity iconEntity1 = new IconEntity();
        iconEntity1.setIconTypeId(1456971922954809346L);
        iconEntity1.setUrl("test url4");
        iconEntityDao.insert(iconEntity1);
    }


    @Autowired
    private IconDao iconDao;

    @Test
    void getOneIcon() {
        Icon icon = iconDao.selectById(1457171882690793474L);
        System.out.println(icon);
    }

    @Autowired
    private IconEntityService service;

    @Test
    void getIcons() {
        System.out.println(service.getIcons(1, 2));
    }

    @Test
    void activeIcons() {
        long[] ids = new long[]{1457171882690793474L, 1457171889028386818L};
        System.out.println(service.activeIcons(ids));
    }

    @Test
    void deletedIcons() {
        long[] ids = new long[]{1457171882690793474L, 1457171889028386818L};
        System.out.println(service.deletedIcons(ids));
    }

    //"icon-gangqin" "icon-15leqi" "icon-huaban" "icon-wangkeshichang" "icon-icon-test" "icon-programmingwindo" "icon-xuexiao"

    // "icon-yiliaoqixie" , "icon-tubiao_-2", "icon-tubiao_-1", "icon--yiliao-Bchao",  "icon--yiliao-huayan" , "icon--yiliao-lunyi",  "icon-tubiao_-" , "icon-Tooth"  ,"icon-yiliao1"
    @Autowired
    private IconEntityDao dao;

    @Test
    void gets() {
//        System.out.println(service.getIcons(1,2));
        Page<IconEntity> page = new Page<>(3, 2);
        QueryWrapper<IconEntity> queryWrapper = new QueryWrapper<>();
        Page<IconEntity> page1 = dao.selectPage(page, queryWrapper);
        List<IconEntity> records = page1.getRecords();
        System.out.println(records);
    }

    @Test
    void demo() {
        String arr[]={ "icon-gangqin", "icon-15leqi", "icon-huaban" ,"icon-wangkeshichang", "icon-icon-test", "icon-programmingwindo", "icon-xuexiao"};
        Long id=1456971922954808598L;
        for(int i=0;i<arr.length;i++){
             IconEntity iconEntity = new IconEntity();
            iconEntity.setUrl(arr[i]);
            iconEntity.setIconTypeId(id);
            iconEntityDao.insert(iconEntity);

        }



    }


}

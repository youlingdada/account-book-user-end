package com.account.book.dao;

import com.account.book.entity.IncomePay;
import com.account.book.entity.IncomePayEntity;
import com.account.book.service.IncomePayService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @author youlingdada youlingdada@163.com
 * @version 1.0
 * @createDate 2021/11/17 20:44
 */
@SpringBootTest
public class IncomePayTest {

    @Autowired
    private IncomePayDao incomePayDao;

    @Autowired
    private IncomePayEntityDao incomePayEntityDao;

    @Test
    void getOneById() {
        System.out.println(incomePayDao.getOneById("1460954028478861314"));
    }

    @Test
    void addOne() {
        IncomePayEntity incomePayEntity = new IncomePayEntity();
        incomePayEntity.setDescription("hello world");
        incomePayEntity.setFlag((byte) 1);
        incomePayEntity.setMoney((double) 0);
        incomePayEntity.setUserId(1456905767577747458L);
        incomePayEntity.setTagId(1459502804177059841L);
        incomePayEntityDao.insert(incomePayEntity);
    }

    @Autowired
    private IncomePayService payService;

    @Test
    void getIncomePaysByList() {
        List<IncomePay> paysList = payService.getIncomePaysList(1462339176035778561L, 0, 10);
        for (IncomePay pay : paysList) {
            System.out.println(pay);
        }
    }

    @Test
    void getIncomePaysByDateList() {
        List<IncomePay> paysList = payService.getIncomePaysByDateList(1462339176035778561L, 0, 10, 2021, 10, 25);
        for (IncomePay pay : paysList) {
            System.out.println(pay);
        }
    }

    @Test
    void getIncomePayMouth() {
        List<IncomePay> paysList = payService.getIncomePaysByMouthList(1462339176035778561L, 0, 10, 2021, 10);
        for (IncomePay pay : paysList) {
            System.out.println(pay);
        }
    }

    @Test
    void getIncomePayByCurrentWeekList(){
        List<IncomePay> paysList = payService.getIncomePayByWeekList(1462339176035778561L, 0, 10, 2021, 10,28);
        for (IncomePay pay : paysList) {
            System.out.println(pay);
        }
    }
}

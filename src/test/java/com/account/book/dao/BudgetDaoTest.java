package com.account.book.dao;

import com.account.book.entity.Budget;
import com.account.book.entity.BudgetEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author youlingdada youlingdada@163.com
 * @version 1.0
 * @createDate 2021/11/10 20:00
 */
@SpringBootTest
public class BudgetDaoTest {
    @Autowired
    private BudgetEntityDao budgetEntityDao;

    @Test
    void addBudget() {
        BudgetEntity budgetEntity = new BudgetEntity();
        budgetEntity.setFlag((byte) 1);
        budgetEntity.setAward("hello world");
        budgetEntity.setMoney(100.20);
        budgetEntity.setUserId(1456905767577747458L);
        budgetEntityDao.insert(budgetEntity);
    }

    @Autowired
    private BudgetDao budgetDao;

    @Test
    void selectOne() {
        Budget budget = budgetDao.selectById(1456959659548205058L);
        System.out.println(budget);
    }

//    @Test
//    void getLastBudget() {
//        Date time = budgetEntityDao.getLastBudgetTime(1456905767577747458L, 0);
//        int month = time.getMonth();
//        int year = time.getYear();
//        System.out.println("fda" + year + "\t" + month);
//        Calendar calendar = Calendar.getInstance();
//        System.out.println(calendar.get(Calendar.YEAR));
//        System.out.println(calendar.get(Calendar.MONTH));
//        Calendar calendar1 = Calendar.getInstance();
//        calendar1.setTime(time);
//        System.out.println(calendar1.get(Calendar.YEAR));
//        System.out.println(calendar1.get(Calendar.MONTH));
//    }
}

package com.account.book.dao;

import com.account.book.entity.*;
import com.account.book.service.TagEntityService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @author youlingdada youlingdada@163.com
 * @version 1.0
 * @createDate 2021/11/10 20:32
 */
@SpringBootTest
public class TagDaoTest {

    @Autowired
    private TagDao tagDao;

    @Autowired
    private TagEntityDao tagEntityDao;

    @Autowired
    private TagEntityService service;
    @Test
    void getOne(){
        final List<TagEntity> tagEntityByUserId = service.getTagEntityByUserId(1456958831051460609L);
        for(TagEntity t:tagEntityByUserId){
            System.out.println(t);
        }
    }





    @Test
    void add(){
//        TagEntity tag = new TagEntity();
//        tag.setName("吃饭");
//        tag.setIconId(1457171882690793474L);
//        tagEntityDao.insert(tag);
        TagEntity t=service.getOneById(1463813470968385537L);
        System.out.println(t);
    }
}

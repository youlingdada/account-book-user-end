package com.account.book.dao;

import com.account.book.entity.Icon;
import com.account.book.entity.IconEntity;
import com.account.book.entity.IconType;
import com.account.book.service.IconEntityService;
import com.account.book.service.IconTypeEntityService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @author youlingdada youlingdada@163.com
 * @version 1.0
 * @createDate 2021/11/7 10:27
 */
@SpringBootTest
public class IconTypeDaoTest {

    @Autowired
    private IconTypeDao iconTypeDao;
    @Autowired
    private IconTypeEntityService iconEntityService;

    @Test
    void getOne() {
        IconType iconType = iconTypeDao.selectIconById(1456971922954809346L);
        List<IconEntity> icons = iconType.getIcons();
        System.out.println(icons.size());
        icons.forEach(System.out::println);
    }

    @Test
    void activeIconTypes() {
        long[] ids = new long[]{1456971922954809346L};
        System.out.println(iconEntityService.activeIconTypes(ids));
    }

    @Test
    void deletedIconTypes() {
        long[] ids = new long[]{1456971922954809346L};
        System.out.println(iconEntityService.deletedIconTypes(ids));
    }
}

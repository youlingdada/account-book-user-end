package com.account.book.dao;

import com.account.book.entity.UserEntity;
import com.account.book.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;

/**
 * @author youlingdada youlingdada@163.com
 * @version 1.0
 * @createDate 2021/11/6 16:31
 */
@SpringBootTest
public class UserEntityDaoTest {

    @Autowired
    private UserEntityDao userEntityDao;

    @Test
    void add() {
        UserEntity user = new UserEntity();
        user.setPhone("15284052715");
        user.setUsername("远方");
        System.out.println(userEntityDao.insert(user));
    }


    @Autowired
    private UserService userService;

    @Test
    void getOne() {
        UserEntity user = userEntityDao.selectById(1456905767577747458L);
        System.out.println(user);
    }

    @Test
    void updateOne() {
        UserEntity user = new UserEntity();
        user.setUId(1456905767577747458L);
        user.setUsername("yxd");
        userEntityDao.updateById(user);
    }

    @Test
    void deletedUsers() {
        long[] ids = new long[]{1456905767577747458L, 1456958831051460609L};
        System.out.println(userService.deleteUsers(ids));
    }

    @Test
    void activeUsers() {
        long[] ids = new long[]{1456905767577747458L, 1456958831051460609L};
        System.out.println(userService.activeUsers(ids));
    }
}

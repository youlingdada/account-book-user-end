package com.account.book;

import com.account.book.dao.BudgetDao;
import com.account.book.dao.BudgetEntityDao;
import com.account.book.dao.IconTypeEntityDao;
import com.account.book.dao.UserEntityDao;
import com.account.book.entity.Budget;
import com.account.book.entity.BudgetEntity;
import com.account.book.entity.IconTypeEntity;
import com.account.book.entity.UserEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
class AccountBookUserEndApplicationTests {

    @Test
    void contextLoads() {
    }

    @Autowired
    private UserEntityDao userEntityDao;

    @Test
    void add() {
        UserEntity user = new UserEntity();
        user.setPhone("15284052715");
        user.setUsername("youlingdada");
        System.out.println(userEntityDao.insert(user));
    }

    @Test
    void getOne() {
        UserEntity user = userEntityDao.selectById(1456905767577747458L);
        System.out.println(user);
    }

    @Test
    void updateOne(){
        UserEntity user = new UserEntity();
        user.setUId(1456905767577747458L);
        user.setUsername("lingdada");
        userEntityDao.updateById(user);
        selectOne();
    }

    @Autowired
    private BudgetEntityDao budgetEntityDao;

    @Test
    void addBudget() {
        BudgetEntity budgetEntity = new BudgetEntity();
        budgetEntity.setFlag((byte) 1);
        budgetEntity.setAward("hello world");
        budgetEntity.setMoney(100.20);
        budgetEntity.setUserId(1456905767577747458L);
        budgetEntityDao.insert(budgetEntity);
    }

    @Autowired
    private BudgetDao budgetDao;

    @Test
    void selectOne() {
        Budget budget = budgetDao.selectById(1456959659548205058L);
        System.out.println(budget);
    }

    @Autowired
    private IconTypeEntityDao iconTypeEntityDao;

    @Test
    void addIconType() {
//        int arr[][]=new int[2][2];
//        Arrays.sort(arr);

        IconTypeEntity type = new IconTypeEntity();
        type.setName("娱乐");
        iconTypeEntityDao.insert(type);
    }

    @Test
    void fun(){
        String []arr=new String[]{null,null};
        for(String i:arr){
            System.out.println(i.length());
        }

    }
}

package com.account.book.handler;

import com.youlingdada.utils.entity.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {
    // 全局异常处理器
    @ExceptionHandler(value = Exception.class)
    public Result exceptionHandler(HttpServletRequest request, Exception e) {
        return Result.getInstance(Result.SERVER_ERROR, "请求:" +
                request.getRequestURI() + ",发生错误,error:" +
                e.getMessage(), e.getCause());
    }

    @ExceptionHandler(value = NullPointerException.class)
    public Result notFindResources(NullPointerException e, HttpServletRequest request) {
        log.info("请求路径：{},错误消息：{}，造成原因：{}", request.getRequestURI(), e.getMessage(), e.getCause());
        return Result.getInstance(Result.SERVER_ERROR, request.getRequestURI() + "请求地址不存在", e.getCause());
    }
}

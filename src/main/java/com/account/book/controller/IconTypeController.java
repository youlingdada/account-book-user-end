package com.account.book.controller;

import com.account.book.entity.IconType;
import com.account.book.service.IconTypeService;
import com.youlingdada.utils.entity.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;




@RequestMapping("/iconType")
@RestController
public class IconTypeController {

    @Autowired
    private IconTypeService iconTypeService;


    @GetMapping(value = "/getIconType/{id}/")
    public Result getIconType(@PathVariable(value = "id") Long id){
        IconType iconType=iconTypeService.getIconTypeById(id);
        if(iconType==null) return Result.getInstance(Result.REQUEST_FAILURE);
        return Result.getInstance(Result.REQUEST_SUCCESS,"请求成功",iconType);

    }
}

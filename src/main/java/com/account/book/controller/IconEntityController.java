package com.account.book.controller;


import com.account.book.entity.IconEntity;
import com.account.book.service.IconEntityService;
import com.youlingdada.utils.entity.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/iconEntity")
public class IconEntityController {

    @Autowired
    private IconEntityService service;

    @PostMapping("/add")
    public Result addIcon(IconEntity icon){
        IconEntity iconEntity = service.addIcon(icon);
        if(iconEntity==null)  return Result.getInstance(Result.REQUEST_FAILURE);
        else return Result.getInstance(Result.REQUEST_SUCCESS,"请求成功",iconEntity);

    }
//
    @GetMapping("/getOneById")
    public Result getOneById(Long iId){
        IconEntity oneById = service.getOneById(iId);
        if(oneById==null) return Result.getInstance(Result.REQUEST_FAILURE);
        else return Result.getInstance(Result.REQUEST_SUCCESS,"请求成功",oneById);
    }
//
    @GetMapping("/getIcons")
    public Result getIcons(int order, int num){
        List<IconEntity> icons = service.getIcons(order, num);
        if(icons==null) return Result.getInstance(Result.REQUEST_FAILURE);
        else return  Result.getInstance(Result.REQUEST_SUCCESS,"请求成功",icons);
    }
//
    @PostMapping("/modify")
     public Result modifyIconById(IconEntity icon){
         IconEntity iconEntity = service.modifyIconById(icon);
         if(iconEntity==null) return Result.getInstance(Result.REQUEST_FAILURE);
         else return Result.getInstance(Result.REQUEST_SUCCESS,"请求成功",iconEntity);
     }
//

    /**
     * 测试未通过
     * @param
     * @return
     */
    @PostMapping("/remove")
    public Result removeIconById(Long id,Long userId){

        boolean flag = service.removeIconById(id, userId);
        if(flag==false) return Result.getInstance(Result.REQUEST_FAILURE);
        else return  Result.getInstance(Result.REQUEST_SUCCESS,"请求成功",flag);
    }





}

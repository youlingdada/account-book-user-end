package com.account.book.controller;

import com.account.book.entity.Tag;
import com.account.book.service.TagService;
import com.youlingdada.utils.entity.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping("/tag")
@RestController
public class TagController {

    @Autowired
    private TagService service;

    //    4表联查 慎用
    @GetMapping("/getOneById")
    public Result getOneById(Long tId){
        Tag tag = service.getOneById(tId);
        if(tag==null) return Result.getInstance(Result.REQUEST_FAILURE);
        else {
            return Result.getInstance(Result.REQUEST_SUCCESS,"请求成功",tag);
        }

    }


    @GetMapping("/getTags/{userId}/")
    public Result getTags(@PathVariable Long userId){
        final List<Tag> tags = service.getTags(userId);
        if(tags==null) return Result.getInstance(Result.REQUEST_FAILURE);
        else return Result.getInstance(Result.REQUEST_SUCCESS,"請求成功",tags);


    }
}

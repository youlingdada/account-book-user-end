package com.account.book.controller;

import com.account.book.entity.TagEntity;
import com.account.book.service.TagEntityService;
import com.youlingdada.utils.entity.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/tagEntity")
public class TagEntityController {

    @Autowired
    private TagEntityService tagEntityService;

    @PostMapping("/add")
    public Result addTagEntity(TagEntity tagEntity){
        TagEntity tagEntity1 = tagEntityService.addTagEntity(tagEntity);
        if(tagEntity1==null){
            return Result.getInstance(Result.REQUEST_FAILURE);
        }else{
            return Result.getInstance(Result.REQUEST_SUCCESS,"请求成功",tagEntity1);
        }

    }

    @PostMapping("/update")
    public Result updateTagEntity(TagEntity tagEntity){
        TagEntity tagEntity1 = tagEntityService.updateTagEntity(tagEntity);
        if(tagEntity1==null) return Result.getInstance(Result.REQUEST_FAILURE);
        else return Result.getInstance(Result.REQUEST_SUCCESS,"请求成功",tagEntity1);
    }

    @PostMapping("/remove")
    public Result removeTagEntityById(Long gId){
        boolean b = tagEntityService.removeTagEntityById(gId);
        if(!b) return Result.getInstance(Result.REQUEST_FAILURE);
        else return Result.getInstance(Result.REQUEST_SUCCESS,"请求成功",b);
    }


    @GetMapping("/getTags/{userId}")
    public Result getTagEntityByUserId(@PathVariable Long userId){
        System.out.println(userId);

        List<TagEntity> list= tagEntityService.getTagEntityByUserId(userId);

        if(list==null) return Result.getInstance(Result.REQUEST_FAILURE);
        else return Result.getInstance(Result.REQUEST_SUCCESS,"请求成功",list);

    }

    @GetMapping("/getOneById/{gId}/")
    public Result getOneById(@PathVariable Long gId){
        TagEntity oneById = tagEntityService.getOneById(gId);
        if(oneById==null) return Result.getInstance(Result.REQUEST_FAILURE);
        else return Result.getInstance(Result.REQUEST_SUCCESS,"请求成功",oneById);
    }




}

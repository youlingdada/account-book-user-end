package com.account.book.controller;

import com.account.book.entity.IncomePayEntity;
import com.account.book.service.IncomePayEntityService;
import com.youlingdada.utils.entity.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/incomePayEntity")
public class IncomePayEntityController {

    @Autowired
    private IncomePayEntityService incomePayEntityService;

    //增加一条收支记录
    @PostMapping("/add")
    public Result addIncomePay(IncomePayEntity incomePayEntity) {
        IncomePayEntity incomePayEntity1 = incomePayEntityService.addIncomePay(incomePayEntity);
        if (incomePayEntity1 == null) {
            return Result.getInstance(Result.REQUEST_FAILURE);
        } else {
            return Result.getInstance(Result.REQUEST_SUCCESS, "请求成功", incomePayEntity1);
        }

    }


    //修改一条收支记录信息
    @PostMapping("/modify")
    public Result modifyIncomePayById(IncomePayEntity incomePayEntity) {
        IncomePayEntity incomePayEntity1 = incomePayEntityService.modifyIncomePayById(incomePayEntity);
        if (incomePayEntity1 == null) {
            return Result.getInstance(Result.REQUEST_FAILURE);
        } else {
            return Result.getInstance(Result.REQUEST_SUCCESS, "请求成功", incomePayEntity1);
        }
    }

    //
    @PostMapping("/remove")
    public Result removeIncomePayById(Long id, Long userId) {
        boolean flag = incomePayEntityService.removeIncomePayById(id, userId);
        if(!flag) return Result.getInstance(Result.REQUEST_FAILURE);
        else return Result.getInstance(Result.REQUEST_SUCCESS,"请求成功",flag);

    }


    @GetMapping("/getOneById")
    public Result getOneById(Long id) {
        IncomePayEntity incomePayEntity = incomePayEntityService.getOneById(id);
        if (incomePayEntity == null) return Result.getInstance(Result.REQUEST_FAILURE);
        else {
            return Result.getInstance(Result.REQUEST_SUCCESS, "请求成功", incomePayEntity);
        }

    }

    @GetMapping("/getIncomePays")
   public Result getIncomePays( int order, int num,Long userId){
        List<IncomePayEntity> list=incomePayEntityService.getIncomePays(userId,order,num);
        if(list==null) return Result.getInstance(Result.REQUEST_FAILURE);
        else return  Result.getInstance(Result.REQUEST_SUCCESS,"请求成功",list);
    }



}

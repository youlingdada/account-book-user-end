package com.account.book.controller;

import com.account.book.entity.BudgetEntity;
import com.account.book.service.BudgetEntityService;
import com.youlingdada.utils.entity.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/budgetEntity")
public class BudgetEntityController {


    @Autowired
    private BudgetEntityService budgetEntityService;

    //添加一条用户预算记录
    @PostMapping("/add")
    public Result addBudget(BudgetEntity budgetEntity) {
        BudgetEntity budgetEntity1 = budgetEntityService.addBudget(budgetEntity);
        if (budgetEntity1 == null) return Result.getInstance(Result.REQUEST_FAILURE);
        return Result.getInstance(Result.REQUEST_SUCCESS, "请求成功", budgetEntity1);

    }

    @PostMapping("/modify")
    public Result modifyBudgetById(BudgetEntity budgetEntity){
        BudgetEntity budgetEntity1=budgetEntityService.modifyBudgetById(budgetEntity);
        if(budgetEntity1==null) return  Result.getInstance(Result.REQUEST_FAILURE);
        return Result.getInstance(Result.REQUEST_SUCCESS,"请求成功",budgetEntity1);

    }

    //删除一条用户预算
    @PostMapping("/remove")
    public Result removeBudgetById(Long id, Long userId) {
        boolean flag = budgetEntityService.removeBudgetById(id, userId);
        if (flag == false) return Result.getInstance(Result.REQUEST_FAILURE);
        return Result.getInstance(Result.REQUEST_SUCCESS, "请求成功", flag);
    }

    //查询用户所有预算
    @GetMapping("/getBudgetByUserId/{id}/")
    public Result getBudgetByUserId(@PathVariable(value = "id") Long id, int order, int num) {
        List<BudgetEntity> list = budgetEntityService.getBudgetByUserId(id, order, num);
        if (list == null) return Result.getInstance(Result.REQUEST_FAILURE);
        return Result.getInstance(Result.REQUEST_SUCCESS, "请求成功", list);

    }

    //获取用户当月的预算
    @GetMapping("/getOneById/{id}")
    public Result getOneById(@PathVariable(value = "id") Long id) {
        BudgetEntity budgetEntity = budgetEntityService.getOneById(id);
        if (budgetEntity == null) return Result.getInstance(Result.REQUEST_FAILURE);
        return Result.getInstance(Result.REQUEST_SUCCESS, "请求成功", budgetEntity);
    }


}

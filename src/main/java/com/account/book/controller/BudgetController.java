package com.account.book.controller;

import com.account.book.entity.Budget;
import com.account.book.service.BudgetService;
import com.youlingdada.utils.entity.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/budge")
public class BudgetController {
    @Autowired
    private BudgetService budgetService;
    @GetMapping(value = "/getBudgetById/{id}/")
    public Result getBudgetById(@PathVariable(value = "id") Long id ){
        Budget budget=budgetService.getBudgetById(id);
        if(budget==null) return Result.getInstance(Result.REQUEST_FAILURE);
        return Result.getInstance(Result.REQUEST_SUCCESS,"请求成功",budget);

    }

    //获取指定年月的支出预算
    @GetMapping("/getBudgetMonthPay")
    public Result getBudgetByUserIdMouthPay(Long userId, int year, int month){
        month=month-1;
        if(year<2021 || month<0 || month>11){
            return Result.getInstance(Result.REQUEST_FAILURE);
        }
        Budget budget = budgetService.getBudgetByUserIdMouthPay(userId, year, month);
        if(budget==null){
            return Result.getInstance(Result.REQUEST_SUCCESS,"暂时还没得数据",budget);
        }else{
            return Result.getInstance(Result.REQUEST_SUCCESS,"请求成功",budget);
        }
    }

    @GetMapping("/getBudgetMonthIncome")
    public Result getBudgetByUserIdMouthIncome(Long userId, int year, int month){
        month=month-1;
        if(year<2021 || month<0 || month>11){
            return Result.getInstance(Result.REQUEST_FAILURE);
        }
        Budget budget = budgetService.getBudgetByUserIdMouthIncome(userId, year, month);
        if(budget==null){
            return Result.getInstance(Result.REQUEST_FAILURE,"暂时还没得数据",budget);
        }else{
            return Result.getInstance(Result.REQUEST_SUCCESS,"请求成功",budget);
        }
    }

    //获取用户当前月份的支出
    @GetMapping("/getCurrentPay/{userId}/")
    public Result getCurrentUserIdPay(@PathVariable Long userId){
        Budget currentUserIdPay = budgetService.getCurrentUserIdPay(userId);
        if(currentUserIdPay==null){
            return Result.getInstance(Result.REQUEST_FAILURE,"暂时还没得数据");
        }else{
            return Result.getInstance(Result.REQUEST_SUCCESS,"请求成功",currentUserIdPay);
        }
    }

    //获取用户当月收入
    @GetMapping("/getCurrentIncome/{userId}/")
    public Result getCurrentUserIdIncome(@PathVariable Long userId){
        Budget currentUserIdIncome = budgetService.getCurrentUserIdIncome(userId);
        if(currentUserIdIncome==null){
            return Result.getInstance(Result.REQUEST_FAILURE,"暂时还没得数据");
        }else{
            return Result.getInstance(Result.REQUEST_SUCCESS,"请求成功",currentUserIdIncome);
        }
    }



}

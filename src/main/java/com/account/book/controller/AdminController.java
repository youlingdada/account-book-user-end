package com.account.book.controller;

import com.account.book.entity.IconEntity;
import com.account.book.entity.IconTypeEntity;
import com.account.book.entity.UserEntity;
import com.account.book.service.IconEntityService;
import com.account.book.service.IconTypeEntityService;
import com.account.book.service.UserService;
import com.youlingdada.utils.entity.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author youlingdada youlingdada@163.com
 * @version 1.0
 * @createDate 2021/11/23 21:51
 */
@RestController
@CrossOrigin
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    private UserService userService;
    @Autowired
    private IconEntityService iconEntityService;
    @Autowired
    private IconTypeEntityService iconTypeEntityService;

    /**
     * 管理员获取正常的所有用户
     *
     * @param order 第几页
     * @param num   每页数据量
     * @return
     */
    @GetMapping("/getUsers/{order}/{num}/")
    public Result getUsers(@PathVariable int order, @PathVariable int num) {
        final List<UserEntity> page = userService.getUserPage(order, num);
        if (page != null) {
            return Result.getInstance(Result.REQUEST_SUCCESS, "查询成功", page);
        }
        return Result.getInstance(Result.REQUEST_FAILURE, "数据请求失败");
    }

    /**
     * 分页获取逻辑删除的用户
     *
     * @param order
     * @param num
     * @return
     */
    @GetMapping("/getDeletedUsers/{order}/{num}/")
    public Result getDeletedUsers(@PathVariable int order, @PathVariable int num) {
        final List<UserEntity> deletedUsers = userService.getDeletedUsers(order, num);
        if (deletedUsers.size() == 0) return Result.getInstance(Result.REQUEST_FAILURE, "数据请求失败");
        return Result.getInstance(Result.REQUEST_SUCCESS, "数据请求成功", deletedUsers);
    }

    /**
     * 批量逻辑删除用户
     *
     * @param ids
     * @return
     */
    @PostMapping("/deletedUsers")
    public Result deletedUsers(@RequestBody long[] ids) {
        final boolean b = userService.deleteUsers(ids);
        if (b) return Result.getInstance(Result.REQUEST_SUCCESS, "操作成功");
        return Result.getInstance(Result.REQUEST_FAILURE, "操作失败");
    }

    /**
     * 批量激活用户
     *
     * @param ids
     * @return
     */
    @PostMapping("/activeUsers")
    public Result activeUsers(@RequestBody long[] ids) {
        final boolean b = userService.deleteUsers(ids);
        if (b) return Result.getInstance(Result.REQUEST_SUCCESS, "操作成功");
        return Result.getInstance(Result.REQUEST_FAILURE, "操作失败");
    }

    /**
     * 获取用户的总数量
     *
     * @return
     */
    @GetMapping("/getCount")
    public Result getUserCount() {
        return Result.getInstance(Result.REQUEST_SUCCESS, "获取用户数成功", userService.getUserCount());
    }

    /**
     * 获取逻辑删除的用户数量
     *
     * @return
     */
    @GetMapping("/getDeletedUsersCount")
    public Result getDeletedUsersCount() {
        return Result.getInstance(Result.REQUEST_SUCCESS, "获取用户数成功", userService.getDeletedUserCount());
    }


    /**
     * 获取所有的IconType
     *
     * @param order
     * @param num
     * @return
     */
    @GetMapping("/getIconTypes/{order}/{num}/")
    public Result getIconTypes(@PathVariable int order, @PathVariable int num) {
        final List<IconTypeEntity> typeList = iconTypeEntityService.getIconTypeList(order, num);
        if (typeList.size() != 0) return Result.getInstance(Result.REQUEST_SUCCESS, "类型请求成功", typeList);
        return Result.getInstance(Result.REQUEST_FAILURE, "请求失败");
    }

    /**
     * 获取逻辑删除的iconType
     *
     * @param order
     * @param num
     * @return
     */
    @GetMapping("/getDeletedIconTypes/{order}/{num}/")
    public Result getDeletedIconTypes(@PathVariable int order, @PathVariable int num) {
        final List<IconTypeEntity> typeList = iconTypeEntityService.getDeletedIconTypeList(order, num);
        if (typeList.size() != 0) return Result.getInstance(Result.REQUEST_SUCCESS, "类型请求成功", typeList);
        return Result.getInstance(Result.REQUEST_FAILURE, "请求失败");
    }

    /**
     * 获取IconType的数量
     *
     * @return
     */
    @GetMapping("/getIconTypeCount")
    public Result getIconTypeCount() {
        int count = iconTypeEntityService.getIconTypeCount();
        return Result.getInstance(Result.REQUEST_SUCCESS, "类型数量请求成功", count);
    }

    /**
     * 获取逻辑删除的IconType的数量
     *
     * @return
     */
    @GetMapping("/getDeletedIconTypeCount")
    public Result getDeletedIconTypeCount() {
        int count = iconTypeEntityService.getDeletedIconTypeCount();
        return Result.getInstance(Result.REQUEST_SUCCESS, "类型数量请求成功", count);
    }


    /**
     * 获取所有的icon
     *
     * @param order
     * @param num
     * @return
     */
    @GetMapping("/getIcons/{order}/{num}/")
    public Result getIcons(@PathVariable int order, @PathVariable int num) {
        List<IconEntity> iconEntities = iconEntityService.getIcons(order, num);
        if (iconEntities != null) return Result.getInstance(Result.REQUEST_SUCCESS, "icon数据请求成功", iconEntities);
        return Result.getInstance(Result.REQUEST_FAILURE, "请求失败");
    }

    /**
     * 批量激活iconType
     *
     * @param ids
     * @return
     */
    @PostMapping("/activeIconTypes")
    public Result activeIconTypes(@RequestBody long[] ids) {
        final boolean b = iconTypeEntityService.activeIconTypes(ids);
        if (b) return Result.getInstance(Result.REQUEST_SUCCESS, "操作成功");
        return Result.getInstance(Result.REQUEST_FAILURE, "操作失败");
    }

    /**
     * 批量逻辑删除iconType
     *
     * @param ids
     * @return
     */
    @PostMapping("/deletedIconTypes")
    public Result deletedIconTypes(@RequestBody long[] ids) {
        final boolean b = iconTypeEntityService.deletedIconTypes(ids);
        if (b) return Result.getInstance(Result.REQUEST_SUCCESS, "操作成功");
        return Result.getInstance(Result.REQUEST_FAILURE, "操作失败");
    }


    /**
     * 获取所有逻辑删除的icon
     *
     * @param order
     * @param num
     * @return
     */
    @GetMapping("/getDeletedIcons/{order}/{num}/")
    public Result getDeletedIcons(@PathVariable int order, @PathVariable int num) {
        List<IconEntity> iconEntities = iconEntityService.getDeletedIcons(order, num);
        if (iconEntities != null) return Result.getInstance(Result.REQUEST_SUCCESS, "icon数据请求成功", iconEntities);
        return Result.getInstance(Result.REQUEST_FAILURE, "请求失败");
    }


    /**
     * 获取所有Icon的数量
     *
     * @return
     */
    @GetMapping("/getIconCount")
    public Result getIconCount() {
        int count = iconEntityService.getIconCount();
        return Result.getInstance(Result.REQUEST_SUCCESS, "请求成功", count);
    }

    /**
     * 获取所有Icon的数量
     *
     * @return
     */
    @GetMapping("/getDeletedIconsCount")
    public Result getDeletedIconsCount() {
        int count = iconEntityService.getDeletedIconsCount();
        return Result.getInstance(Result.REQUEST_SUCCESS, "请求成功", count);
    }

    /**
     * 批量激活icon
     *
     * @param ids
     * @return
     */
    @PostMapping("/activeIcons")
    public Result activeIcons(@RequestBody long[] ids) {
        final boolean b = iconEntityService.activeIcons(ids);
        if (b) return Result.getInstance(Result.REQUEST_SUCCESS, "操作成功");
        return Result.getInstance(Result.REQUEST_FAILURE, "操作失败");
    }

    /**
     * 批量逻辑删除icon
     *
     * @param ids
     * @return
     */
    @PostMapping("/deletedIcons")
    public Result deletedIcons(@RequestBody long[] ids) {
        final boolean b = iconEntityService.deletedIcons(ids);
        if (b) return Result.getInstance(Result.REQUEST_SUCCESS, "操作成功");
        return Result.getInstance(Result.REQUEST_FAILURE, "操作失败");
    }

    /**
     * 添加一个icon
     */
    @PostMapping("/addIcon")
    public Result addIcon(@RequestBody IconEntity icon) {
        final IconEntity iconEntity = iconEntityService.addIcon(icon);
        if (iconEntity != null) return Result.getInstance(Result.REQUEST_SUCCESS, "图标添加成功", iconEntity);
        return Result.getInstance(Result.REQUEST_FAILURE, "图标添加失败");
    }

    /**
     * 修改图标
     */
    @PostMapping("/changeIcon")
    public Result changeIcon(@RequestBody IconEntity icon) {
        final IconEntity entity = iconEntityService.modifyIconById(icon);
        if (entity != null) return Result.getInstance(Result.REQUEST_SUCCESS, "修改图标成功", entity);
        return Result.getInstance(Result.REQUEST_FAILURE, "图标修改失败");
    }

    /**
     * 添加图标类型
     *
     * @param iconTypeEntity
     * @return
     */
    @PostMapping("/addIconType")
    public Result addIconType(@RequestBody IconTypeEntity iconTypeEntity) {
        final IconTypeEntity type = iconTypeEntityService.addIconType(iconTypeEntity);
        if (type != null) return Result.getInstance(Result.REQUEST_SUCCESS, "图标类型添加成功", type);
        return Result.getInstance(Result.REQUEST_FAILURE, "图标类型添加失败");
    }

    /**
     * 修改图标类型
     *
     * @param iconTypeEntity
     * @return
     */
    @PostMapping("/changeIconType")
    public Result changeIconType(@RequestBody IconTypeEntity iconTypeEntity, @RequestBody Long tId) {
        iconTypeEntity.setTId(tId);
        final IconTypeEntity type = iconTypeEntityService.modifyIconTypeById(iconTypeEntity);
        if (type != null) return Result.getInstance(Result.REQUEST_SUCCESS, "图标类型修改成功", type);
        return Result.getInstance(Result.REQUEST_FAILURE, "图标类型修改失败");

    }


}

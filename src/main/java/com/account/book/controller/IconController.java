package com.account.book.controller;


import com.account.book.entity.Icon;
import com.account.book.service.IconService;
import com.youlingdada.utils.entity.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/icon")
public class IconController {

    @Autowired
    private IconService service;


    @GetMapping("/selectOneById")
    public Result selectOneById(Long id){
        Icon icon = service.selectOneById(id);
        if(icon==null) return Result.getInstance(Result.REQUEST_FAILURE);
        else return Result.getInstance(Result.REQUEST_SUCCESS,"请求成功",icon);
    }


}

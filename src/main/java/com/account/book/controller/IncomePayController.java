package com.account.book.controller;

import com.account.book.entity.IncomePay;
import com.account.book.service.IncomePayService;
import com.youlingdada.utils.entity.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/incomePay")
public class IncomePayController {

    @Autowired
    private IncomePayService service;


    @GetMapping("/getOneById")
    public Result getOneById(Long pId) {
        IncomePay incomePay = service.getOneById(pId);
        if (incomePay == null) return Result.getInstance(Result.REQUEST_FAILURE);
        else {
            return Result.getInstance(Result.REQUEST_SUCCESS, "请求成功", incomePay);
        }
    }

    // 获取用户的incomePay 按照时间降序排
    @GetMapping("/getIncomePays/{userId}/{order}/{num}/")
    public Result getIncomePays(@PathVariable long userId, @PathVariable int order, @PathVariable int num) {
        List<IncomePay> incomePaysList = service.getIncomePaysList(userId, order, num);
        if (incomePaysList.size() == 0) return Result.getInstance(Result.REQUEST_FAILURE, "获取数据失败,数据没有了");
        return Result.getInstance(Result.REQUEST_SUCCESS, "获取数据成功", incomePaysList);
    }

    // 按照具体日期获取
    @GetMapping("/getIncomePaysByDate/{userId}/{order}/{num}/{year}/{mouth}/{date}/")
    public Result getIncomePaysByDate(@PathVariable long userId, @PathVariable int order, @PathVariable int num, @PathVariable int year, @PathVariable int mouth, @PathVariable int date) {
        mouth -= 1;
        List<IncomePay> incomePaysList = service.getIncomePaysByDateList(userId, order, num, year, mouth, date);
        if (incomePaysList.size() == 0) return Result.getInstance(Result.REQUEST_FAILURE, "获取数据失败,数据没有了");
        return Result.getInstance(Result.REQUEST_SUCCESS, "获取数据成功", incomePaysList);
    }

    // 按照具体月份获取
    @GetMapping("/getIncomePaysMouth/{userId}/{order}/{num}/{year}/{mouth}/")
    public Result getIncomePayMouth(@PathVariable long userId, @PathVariable int order, @PathVariable int num, @PathVariable int year, @PathVariable int mouth) {
        mouth -= 1;
        List<IncomePay> incomePaysList = service.getIncomePaysByMouthList(userId, order, num, year, mouth);
        if (incomePaysList.size() == 0) return Result.getInstance(Result.REQUEST_FAILURE, "获取数据失败,数据没有了");
        return Result.getInstance(Result.REQUEST_SUCCESS, "获取数据成功", incomePaysList);
    }

    // 查询指定周的收支记录
    @GetMapping("/getIncomePaysByWeek/{userId}/{order}/{num}/{year}/{mouth}/{date}/")
    public Result getIncomePayByWeekList(@PathVariable long userId, @PathVariable int order, @PathVariable int num, @PathVariable int year, @PathVariable int mouth, @PathVariable int date) {
        mouth -= 1;
        List<IncomePay> incomePaysList = service.getIncomePayByWeekList(userId, order, num, year, mouth, date);
        if (incomePaysList.size() == 0) return Result.getInstance(Result.REQUEST_FAILURE, "获取数据失败,数据没有了");
        return Result.getInstance(Result.REQUEST_SUCCESS, "获取数据成功", incomePaysList);
    }

    // 查询指定月份的所有收入账单数
    @GetMapping("/getIncomeCountMouth/{userId}/{year}/{mouth}/")
    public Result getIncomeCountMouth(@PathVariable long userId, @PathVariable int year, @PathVariable int mouth) {
        final Integer countMouth = service.getIncomeCountMouth(userId, year, mouth - 1);
        return Result.getInstance(Result.REQUEST_SUCCESS, "请求成功", countMouth);
    }

    // 查询指定月份的所有支出账单数
    @GetMapping("/getPayCountMouth/{userId}/{year}/{mouth}/")
    public Result getPayCountMouth(@PathVariable long userId, @PathVariable int year, @PathVariable int mouth) {
        final Integer countMouth = service.getPayCountMouth(userId, year, mouth - 1);
        return Result.getInstance(Result.REQUEST_SUCCESS, "请求成功", countMouth);
    }

    // 查询指定周的所有收入账单数
    @GetMapping("/getIncomeCountWeek/{userId}/{year}/{mouth}/{date}/")
    public Result getIncomeCountWeek(@PathVariable long userId, @PathVariable int year, @PathVariable int mouth, @PathVariable int date) {
        final Integer countMouth = service.getIncomeCountWeek(userId, year, mouth - 1, date);
        return Result.getInstance(Result.REQUEST_SUCCESS, "请求成功", countMouth);
    }

    // 查询指定周的所有支出账单数
    @GetMapping("/getPayCountWeek/{userId}/{year}/{mouth}/{date}/")
    public Result getPayCountWeek(@PathVariable long userId, @PathVariable int year, @PathVariable int mouth, @PathVariable int date) {
        final Integer countMouth = service.getPayCountWeek(userId, year, mouth - 1, date);
        return Result.getInstance(Result.REQUEST_SUCCESS, "请求成功", countMouth);
    }
}

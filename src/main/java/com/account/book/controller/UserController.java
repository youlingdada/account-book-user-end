package com.account.book.controller;

import com.account.book.entity.UserEntity;
import com.account.book.service.UserService;
import com.account.book.utils.UserUtils;
import com.youlingdada.utils.entity.Result;
import com.youlingdada.utils.tools.security.JwtUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;


@RequestMapping("/user")
@RestController
@Slf4j
public class UserController {

    @Autowired
    private UserService userService;


    //登录
    @PostMapping("/login")
    public Result login(String phone) {
        log.info("登录成功，电话号码为：{}", phone);
        UserEntity userEntity = userService.getUserByPhone(phone);
        if (userEntity == null) return Result.getInstance(Result.REQUEST_FAILURE);
        String token = JwtUtils.getToken(UserUtils.toMap(userEntity));
        return Result.getInstance(Result.REQUEST_SUCCESS, "请求成功", token);

    }

    //注册用户
    @PostMapping("/register")
    public Result register(UserEntity userEntity) {

        UserEntity userEntity1 = userService.addUser(userEntity);
        if (userEntity1 == null) {
            return Result.getInstance(Result.REQUEST_FAILURE);
        }
        String token = JwtUtils.getToken(UserUtils.toMap(userEntity1));
        return Result.getInstance(Result.REQUEST_SUCCESS, "请求成功", token);
    }

    //    Duplicate
    //修改用户
    @PostMapping("/modify")
    public Result modifyUser(UserEntity userEntity) {
        UserEntity userEntity1 = userService.modifyUser(userEntity);
        if (userEntity1 == null) return Result.getInstance(Result.REQUEST_FAILURE);
        String token = JwtUtils.getToken(UserUtils.toMap(userEntity1));
        return Result.getInstance(Result.REQUEST_SUCCESS, "修改成功", token);
    }
}

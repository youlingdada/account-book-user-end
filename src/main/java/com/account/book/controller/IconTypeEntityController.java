package com.account.book.controller;

import com.account.book.entity.IconTypeEntity;
import com.account.book.service.IconTypeEntityService;
import com.youlingdada.utils.entity.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/iconTypeEntity")
public class IconTypeEntityController {

    @Autowired
    private IconTypeEntityService service;

    //    添加一个IconType
    @PostMapping("/add")
    public Result addIconType(IconTypeEntity iconTypeEntity){
        IconTypeEntity iconTypeEntity1 = service.addIconType(iconTypeEntity);
        if(iconTypeEntity1==null){
            return Result.getInstance(Result.REQUEST_FAILURE);
        }else{
            return Result.getInstance(Result.REQUEST_SUCCESS,"请求成功",iconTypeEntity1);
        }
    }

    //    按照id更新一个IconType
    @PostMapping("/modify")
    public Result modifyIconTypeById(IconTypeEntity iconTypeEntity){
        IconTypeEntity iconTypeEntity1 = service.modifyIconTypeById(iconTypeEntity);
        if(iconTypeEntity1==null){
            return Result.getInstance(Result.REQUEST_FAILURE);
        }else {
            return Result.getInstance(Result.REQUEST_FAILURE,"请求成功",iconTypeEntity1);
        }
    }



//
//    //    获取全部的IconType
    @GetMapping("/getIconTypeList")
     public Result getIconTypeList(){
         List<IconTypeEntity> iconTypeList = service.getIconTypeList();
         if(iconTypeList==null) return Result.getInstance(Result.REQUEST_FAILURE);
         else return Result.getInstance(Result.REQUEST_SUCCESS,"请求成功",iconTypeList);
     }


    @PostMapping("/remove")
    public Result removeIconTypeId(Long tId){
        boolean flag = service.removeIconTypeId(tId);
        if(flag==false) return Result.getInstance(Result.REQUEST_FAILURE);
        else return Result.getInstance(Result.REQUEST_SUCCESS,"请求成功",flag);
    }

}

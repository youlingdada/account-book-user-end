package com.account.book.utils;

import com.account.book.entity.UserEntity;
import org.apache.catalina.User;

import java.util.HashMap;
import java.util.Map;

/**
 * @author youlingdada youlingdada@163.com
 * @version 1.0
 * @createDate 2021/11/10 21:02
 */
public class UserUtils {
    public static Map<String, String> toMap(UserEntity user) {
        Map<String, String> userMap = new HashMap<>();
        userMap.put("id", String.valueOf(user.getUId()));
        userMap.put("username", user.getUsername());
        userMap.put("phone", user.getPhone());
        userMap.put("pay", String.valueOf(user.getPay()));
        userMap.put("income", String.valueOf(user.getIncome()));
        userMap.put("money", String.valueOf(user.getMoney()));
        userMap.put("deleted", String.valueOf(user.getDeleted()));
        userMap.put("version", String.valueOf(user.getVersion()));
        userMap.put("createTime", String.valueOf(user.getCreateTime()));
        userMap.put("updateTime", String.valueOf(user.getUpdateTime()));
        return userMap;
    }

    public static UserEntity toUser(Map<String, String> map) {
        return new UserEntity();
    }
}

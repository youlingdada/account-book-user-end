package com.account.book.utils;


import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * @author youlingdada youlingdada@163.com
 * @version 1.0
 * @createDate 2021/11/11 14:34
 */
public final class TookUtils<T> {
    public Map<String, Object> toMap(T t) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Map<String, Object> map = new HashMap<>();
        Method method;
        for (Field declaredField : t.getClass().getDeclaredFields()) {
            String name = declaredField.getName();
            name = String.valueOf(name.charAt(0)).toUpperCase() + name.substring(1);
            method = t.getClass().getMethod("get" + name, (Class<?>[]) null);
            map.put(declaredField.getName(), method.invoke(t, (Object[]) null));
        }
        return map;
    }
}

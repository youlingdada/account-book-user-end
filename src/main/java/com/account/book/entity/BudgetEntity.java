package com.account.book.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * @author youlingdada youlingdada@163.com
 * @version 1.0
 * @createDate 2021/11/1 21:57
 */
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@TableName("budget")
public final class BudgetEntity implements Serializable {
    //    id
    @TableId(type = IdType.ASSIGN_ID)
    private Long bId;
    //    预算的金额
    private Double money;
    //    当前金额
    @TableField(fill = FieldFill.INSERT)
    private Double currentMoney;
    //    完成的奖励
    private String award;
    //    所属的用户
    private Long userId;
    //    flag 0 -> 收入 1 -> 支出
    private Byte flag;
    //    乐观锁
    @Version
    private Integer version;
    //    创建时间
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    //    更新时间
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
}
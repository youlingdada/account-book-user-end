package com.account.book.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * @author youlingdada youlingdada@163.com
 * @version 1.0
 * @createDate 2021/11/1 18:11
 */
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public final class Icon implements Serializable {
    //    id
    @TableId(type = IdType.ASSIGN_ID)
    private Long iId;
    //    icon 图标链接
    private String url;
    //    icon类型
    private IconTypeEntity iconType;
    //    逻辑删除
    @TableLogic
    private Integer deleted;
    //    乐观锁
    @Version
    private Integer version;
    //    创建时间
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    //    更新时间
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
}

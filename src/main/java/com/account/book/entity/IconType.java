package com.account.book.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author youlingdada youlingdada@163.com
 * @version 1.0
 * @createDate 2021/11/6 21:11
 */
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@TableName("icon_type")
public final class IconType implements Serializable {
    //    id
    @TableId(type = IdType.ASSIGN_ID)
    private Long tId;
    //    类别名字
    private String name;
    //    逻辑删除
    @TableLogic
    private Integer deleted;
    //    乐观锁
    @Version
    private Integer version;
    //    创建时间
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    //    更新时间
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
    //   设置非数据库字段
    //    类别下的icon
    @TableField(exist = false)
    private List<IconEntity> icons;
}

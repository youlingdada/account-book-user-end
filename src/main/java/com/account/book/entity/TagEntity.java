package com.account.book.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * @author youlingdada youlingdada@163.com
 * @version 1.0
 * @createDate 2021/11/1 18:33
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@TableName("tag")
public final class TagEntity implements Serializable {
    //    id
    @TableId(type = IdType.ASSIGN_ID)
    private Long gId;
    //    用户备注
    private String name;
    //    标签的图标
    private Long iconId;
    //    乐观锁
    private Long userId;
    @Version
    private Integer version;
    //    创建时间
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    //    修改时间
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
}
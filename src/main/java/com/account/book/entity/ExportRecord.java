package com.account.book.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * @author youlingdada youlingdada@163.com
 * @version 1.0
 * @createDate 2021/11/1 18:11
 */
public final class ExportRecord implements Serializable {
    private long id;
    private Double money;
    private String description; // 备注
    private String iconName;
    private String TagClassName; // 大类别名字
    private String typeName; // 支出、收入
    private Date time;
}

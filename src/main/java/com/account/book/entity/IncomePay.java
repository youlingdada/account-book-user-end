package com.account.book.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * @author youlingdada youlingdada@163.com
 * @version 1.0
 * @createDate 2021/11/1 18:10
 */

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@TableName("income_pay")
public final class IncomePay implements Serializable {
    //    id
    @TableId(type = IdType.ASSIGN_ID)
    private Long pId;
    //    这条记录所对应的标签id
    private Tag tag;
    //    对记录的描述
    private String description;
    //    具体金额
    private Double money;
    //    所属用户
    private UserEntity user;
    //    标识支出还是收入 0 -> 收入 1 ->支出
    private Byte flag;
    //    乐观锁
    @Version
    private Integer version;
    //    创建时间
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    //    更新时间
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
}

package com.account.book.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * @author youlingdada youlingdada@163.com
 * @version 1.0
 * @createDate 2021/11/1 18:09
 */
@Data
@AllArgsConstructor
@ToString
@NoArgsConstructor
@TableName("user")
public final class UserEntity implements Serializable {
    //    id
    @TableId(type = IdType.ASSIGN_ID)
    private Long uId;
    //    微信号 微信绑定的手机号
    private String phone;
    //    微信用户名
    private String username;
    //    支出总量
    @TableField(fill = FieldFill.INSERT)
    private Double pay;
    //    收入总量
    @TableField(fill = FieldFill.INSERT)
    private Double income;
    //    当前所剩金额
    @TableField(fill = FieldFill.INSERT)
    private Double money;
    //    逻辑删除
    @TableLogic
    private Integer deleted;
    //    乐观锁
    @Version
    private Integer version;
    //    创建时间
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    //    修改时间
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
}

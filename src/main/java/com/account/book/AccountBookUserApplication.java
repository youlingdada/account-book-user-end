package com.account.book;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
@MapperScan("com.account.book.dao")
public class AccountBookUserApplication {

    public static void main(String[] args) {
        SpringApplication.run(AccountBookUserApplication.class, args);
    }

}

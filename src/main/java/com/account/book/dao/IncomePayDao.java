package com.account.book.dao;

import com.account.book.entity.IncomePay;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author youlingdada youlingdada@163.com
 * @version 1.0
 * @createDate 2021/11/10 21:31
 */
@Mapper
@Repository
public interface IncomePayDao {
    IncomePay getOneById(Serializable pId);

    List<IncomePay> getIncomePaysByTime(long userId, int offset, int num, Date beforeDate, Date afterDate);

    List<IncomePay> getIncomePays(long userId, int offset, int num);

    Integer getIncomePayCountByTime(long userId,Date beforeDate, Date afterDate, Byte flag);



}

package com.account.book.dao;

import com.account.book.entity.UserEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author youlingdada youlingdada@163.com
 * @version 1.0
 * @createDate 2021/11/6 16:23
 */
@Mapper
@Repository
public interface UserEntityDao extends BaseMapper<UserEntity> {
    List<UserEntity> selectDeletedUser(int offset, int num);

    int selectDeletedCount();

    int activeUsers(List<Long> ids);


    int deletedUsers(List<Long> ids);
}

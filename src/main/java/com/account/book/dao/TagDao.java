package com.account.book.dao;

import com.account.book.entity.Tag;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;

/**
 * @author youlingdada youlingdada@163.com
 * @version 1.0
 * @createDate 2021/11/6 19:17
 */
@Mapper
@Repository
public interface TagDao {
    Tag selectById(Serializable gId);

    List<Tag> getTags(Serializable userId);


}

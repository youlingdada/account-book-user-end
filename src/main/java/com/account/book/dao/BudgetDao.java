package com.account.book.dao;

import com.account.book.entity.Budget;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.Date;

/**
 * @author youlingdada youlingdada@163.com
 * @version 1.0
 * @createDate 2021/11/6 19:18
 */
@Mapper
@Repository
public interface BudgetDao {
    // 按照b_id 查询一条预算
    Budget selectById(Serializable id);

    // 按照userId 与 flag 、时间 查询一条预算
    Budget selectMouthByUserId(Serializable userId, byte flag, Date beforeDate, Date afterDate);
}

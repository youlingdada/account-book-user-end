package com.account.book.dao;

import com.account.book.entity.IconType;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

/**
 * @author youlingdada youlingdada@163.com
 * @version 1.0
 * @createDate 2021/11/7 10:05
 */
@Mapper
@Repository
public interface IconTypeDao {
    IconType selectIconById(Serializable id);
}

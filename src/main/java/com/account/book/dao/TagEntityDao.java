package com.account.book.dao;

import com.account.book.entity.TagEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author youlingdada youlingdada@163.com
 * @version 1.0
 * @createDate 2021/11/6 16:24
 */
@Mapper
@Repository
public interface TagEntityDao extends BaseMapper<TagEntity> {
}

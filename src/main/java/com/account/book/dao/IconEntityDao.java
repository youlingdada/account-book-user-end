package com.account.book.dao;

import com.account.book.entity.IconEntity;
import com.account.book.entity.UserEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author youlingdada youlingdada@163.com
 * @version 1.0
 * @createDate 2021/11/6 16:24
 */
@Mapper
@Repository
public interface IconEntityDao extends BaseMapper<IconEntity> {
    List<IconEntity> selectDeletedIcon(int offset, int num);

    int selectDeletedIconCount();

    int activeIcons(List<Long> ids);

    int deletedIcons(List<Long> ids);
}

package com.account.book.dao;

import com.account.book.entity.Icon;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

/**
 * @author youlingdada youlingdada@163.com
 * @version 1.0
 * @createDate 2021/11/6 19:17
 */
@Repository
@Mapper
public interface IconDao {
    Icon selectById(Serializable id);
}

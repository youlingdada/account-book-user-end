package com.account.book.dao;

import com.account.book.entity.BudgetEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.Date;

/**
 * @author youlingdada youlingdada@163.com
 * @version 1.0
 * @createDate 2021/11/6 16:25
 */
@Mapper
@Repository
public interface BudgetEntityDao extends BaseMapper<BudgetEntity> {
    Date getLastBudgetTime(Serializable userId, byte flag);

    Date getLastBudgetTime(long userId, byte flag);
}

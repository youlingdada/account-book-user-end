package com.account.book.service;

import com.account.book.entity.IncomePayEntity;

import java.io.Serializable;
import java.util.List;

/**
 * @author youlingdada youlingdada@163.com
 * @version 1.0
 * @createDate 2021/11/12 16:58
 */
public interface IncomePayEntityService {

    IncomePayEntity addIncomePay(IncomePayEntity incomePayEntity);

    IncomePayEntity modifyIncomePayById(IncomePayEntity incomePayEntity);

    boolean removeIncomePayById(Serializable id,Serializable userId);

    IncomePayEntity getOneById(Serializable id);

    List<IncomePayEntity> getIncomePays(Serializable userId,int order,int num);
}

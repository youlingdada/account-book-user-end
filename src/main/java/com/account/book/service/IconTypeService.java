package com.account.book.service;

import com.account.book.entity.IconType;


import java.io.Serializable;

/**
 * @author youlingdada youlingdada@163.com
 * @version 1.0
 * @createDate 2021/11/10 20:52
 */
public interface IconTypeService {
    IconType getIconTypeById(Serializable id);

}

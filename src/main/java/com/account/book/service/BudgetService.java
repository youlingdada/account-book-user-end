package com.account.book.service;

import com.account.book.entity.Budget;

import java.io.Serializable;

/**
 * @author youlingdada youlingdada@163.com
 * @version 1.0
 * @createDate 2021/11/11 15:56
 */
public interface BudgetService {
    Budget getBudgetById(Serializable id);

    Budget getBudgetByUserIdMouthPay(Serializable userId, int year, int mouth);

    Budget getBudgetByUserIdMouthIncome(Serializable userId, int year, int mouth);

    // 获取当前月的支出预算
    Budget getCurrentUserIdPay(Serializable userId);

    // 获取当前月收入预算
    Budget getCurrentUserIdIncome(Serializable userId);
}

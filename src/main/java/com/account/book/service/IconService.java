package com.account.book.service;

import com.account.book.entity.Icon;

import java.io.Serializable;

/**
 * @author youlingdada youlingdada@163.com
 * @version 1.0
 * @createDate 2021/11/12 17:00
 */
public interface IconService {
    Icon selectOneById(Serializable id);
}

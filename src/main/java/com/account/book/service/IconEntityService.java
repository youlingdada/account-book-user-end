package com.account.book.service;

import com.account.book.entity.IconEntity;

import java.io.Serializable;
import java.util.List;

/**
 * @author youlingdada youlingdada@163.com
 * @version 1.0
 * @createDate 2021/11/12 16:58
 */
public interface IconEntityService {
    IconEntity addIcon(IconEntity icon);

    IconEntity getOneById(Serializable iId);

    IconEntity modifyIconById(IconEntity icon);

    boolean removeIconById(Serializable id, Serializable userId);

    // 获取正常的icon
    List<IconEntity> getIcons(int order, int num);

    // 获取逻辑删除的icon
    List<IconEntity> getDeletedIcons(int order, int num);

    // 获取正常icon数量
    int getIconCount();

    // 获取逻辑删除后的icon数量
    int getDeletedIconsCount();

    // 激活icon
    boolean activeIcons(long[] ids);

    // 逻辑删除icon
    boolean deletedIcons(long[] ids);
}

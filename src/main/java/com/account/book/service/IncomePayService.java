package com.account.book.service;

import com.account.book.entity.IncomePay;

import java.io.Serializable;
import java.util.List;

/**
 * @author youlingdada youlingdada@163.com
 * @version 1.0
 * @createDate 2021/11/12 17:00
 */
public interface IncomePayService {

    IncomePay getOneById(Serializable pId);

    List<IncomePay> getIncomePaysList(long userId, int order, int num);

    // 获取某一天账单
    List<IncomePay> getIncomePaysByDateList(long userId, int order, int num, int year, int mouth, int date);

    // 获取某一月账单
    List<IncomePay> getIncomePaysByMouthList(long userId, int order, int num, int year, int mouth);

    // 获取某一月账单
    List<IncomePay> getIncomePayByWeekList(long userId, int order, int num, int year, int mouth, int date);


    Integer getIncomeCountMouth(long userId, int year, int mouth);

    Integer getPayCountMouth(long userId, int year, int mouth);

    Integer getIncomeCountWeek(long userId, int year, int mouth, int date);

    Integer getPayCountWeek(long userId, int year, int mouth, int date);
}

package com.account.book.service;

import com.account.book.entity.UserEntity;

import java.io.Serializable;
import java.util.List;

/**
 * @author youlingdada youlingdada@163.com
 * @version 1.0
 * @createDate 2021/11/10 20:35
 */
public interface UserService {
    UserEntity getUserByUsername(String username);

    UserEntity getUserByPhone(String phone);

    UserEntity getUserById(Serializable id);

    UserEntity addUser(UserEntity user);

    UserEntity modifyUser(UserEntity user);

    //    获取一页的用户
    List<UserEntity> getUserPage(int order, int num);

    // 获取正常用户的数量
    int getUserCount();

    // 获取逻辑删除的用户
    List<UserEntity> getDeletedUsers(int order, int num);

    // 获取逻辑删除的用户数
    int getDeletedUserCount();

    // 批量逻辑删除用户
    boolean deleteUsers(long[] ids);

    // 逻辑激活用户
    boolean activeUsers(long[] ids);
}

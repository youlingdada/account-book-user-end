package com.account.book.service.impl;

import com.account.book.dao.IncomePayDao;
import com.account.book.entity.IncomePay;
import com.account.book.service.IncomePayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author youlingdada youlingdada@163.com
 * @version 1.0
 * @createDate 2021/11/12 17:01
 */
@Service
public class IncomePayServiceImpl implements IncomePayService {

    @Autowired
    private IncomePayDao incomePayDao;

    @Override
    public IncomePay getOneById(Serializable pId) {
        return incomePayDao.getOneById(pId);
    }

    @Override
    public List<IncomePay> getIncomePaysList(long userId, int order, int num) {
        return incomePayDao.getIncomePays(userId, order, num);
    }

    @Override
    public List<IncomePay> getIncomePaysByDateList(long userId, int order, int num, int year, int mouth, int date) {
        Calendar temp = Calendar.getInstance();
        temp.set(year, mouth, date, 0, 0, 0);
        Date beforeDate, afterDate;
        beforeDate = temp.getTime();
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, mouth, date, 23, 59, 59);
        afterDate = calendar.getTime();
        System.out.println(beforeDate);
        System.out.println(afterDate);
        int offset = order * num;
        return incomePayDao.getIncomePaysByTime(userId, offset, num, beforeDate, afterDate);
    }

    @Override
    public List<IncomePay> getIncomePaysByMouthList(long userId, int order, int num, int year, int mouth) {
        Calendar temp = Calendar.getInstance();
        temp.set(year, mouth, 0, 0, 0, 0);
        Date beforeDate, afterDate;
        beforeDate = temp.getTime();
        Calendar calendar = Calendar.getInstance();
        int maximum = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        calendar.set(year, mouth, maximum, 23, 59, 59);
        afterDate = calendar.getTime();
        int offset = order * num;
        return incomePayDao.getIncomePaysByTime(userId, offset, num, beforeDate, afterDate);
    }

    @Override
    public List<IncomePay> getIncomePayByWeekList(long userId, int order, int num, int year, int mouth, int date) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, mouth, date, 23, 59, 59);
        Date beforeDate, afterDate;
        afterDate = calendar.getTime();
        int weekDay = calendar.get(Calendar.DAY_OF_WEEK);
        if (weekDay == 1) { // 当前日期为星期天
            calendar.add(Calendar.DATE, -6);
        } else {
            calendar.add(Calendar.DATE, -weekDay + 2);
        }
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        beforeDate = calendar.getTime();
        int offset = order * num;
        return incomePayDao.getIncomePaysByTime(userId, offset, num, beforeDate, afterDate);
    }

    @Override
    public Integer getIncomeCountMouth(long userId, int year, int mouth) {
        Calendar temp = Calendar.getInstance();
        temp.set(year, mouth, 0, 0, 0, 0);
        Date beforeDate, afterDate;
        beforeDate = temp.getTime();
        Calendar calendar = Calendar.getInstance();
        int maximum = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        calendar.set(year, mouth, maximum, 23, 59, 59);
        afterDate = calendar.getTime();
        return incomePayDao.getIncomePayCountByTime(userId, beforeDate, afterDate, (byte) 0);
    }

    @Override
    public Integer getPayCountMouth(long userId, int year, int mouth) {
        Calendar temp = Calendar.getInstance();
        temp.set(year, mouth, 0, 0, 0, 0);
        Date beforeDate, afterDate;
        beforeDate = temp.getTime();
        Calendar calendar = Calendar.getInstance();
        int maximum = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        calendar.set(year, mouth, maximum, 23, 59, 59);
        afterDate = calendar.getTime();
        return incomePayDao.getIncomePayCountByTime(userId, beforeDate, afterDate, (byte) 1);
    }

    @Override
    public Integer getIncomeCountWeek(long userId, int year, int mouth, int date) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, mouth, date, 23, 59, 59);
        Date beforeDate, afterDate;
        afterDate = calendar.getTime();
        int weekDay = calendar.get(Calendar.DAY_OF_WEEK);
        if (weekDay == 1) { // 当前日期为星期天
            calendar.add(Calendar.DATE, -6);
        } else {
            calendar.add(Calendar.DATE, -weekDay + 2);
        }
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        beforeDate = calendar.getTime();
        return incomePayDao.getIncomePayCountByTime(userId, beforeDate, afterDate, (byte) 0);
    }

    @Override
    public Integer getPayCountWeek(long userId, int year, int mouth, int date) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, mouth, date, 23, 59, 59);
        Date beforeDate, afterDate;
        afterDate = calendar.getTime();
        int weekDay = calendar.get(Calendar.DAY_OF_WEEK);
        if (weekDay == 1) { // 当前日期为星期天
            calendar.add(Calendar.DATE, -6);
        } else {
            calendar.add(Calendar.DATE, -weekDay + 2);
        }
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        beforeDate = calendar.getTime();
        return incomePayDao.getIncomePayCountByTime(userId, beforeDate, afterDate, (byte) 1);
    }
}

package com.account.book.service.impl;

import com.account.book.dao.TagEntityDao;
import com.account.book.entity.TagEntity;
import com.account.book.service.TagEntityService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;

/**
 * @author youlingdada youlingdada@163.com
 * @version 1.0
 * @createDate 2021/11/12 16:41
 */
@Service
public class TagEntityServiceImpl implements TagEntityService {


    @Autowired
    private TagEntityDao tagEntityDao;

    @Override
    public TagEntity addTagEntity(TagEntity tagEntity) {
        if (tagEntityDao.insert(tagEntity) > 0) return tagEntity;
        return null;
    }

    @Override
    public TagEntity updateTagEntity(TagEntity tagEntity) {
        if (tagEntityDao.updateById(tagEntity) > 0) return tagEntity;
        return null;
    }

    @Override
    public boolean removeTagEntityById(Serializable tId) {
        return tagEntityDao.deleteById(tId) > 0;
    }

    @Override
    public List<TagEntity> getTagEntityByUserId(Serializable userId) {
        QueryWrapper<TagEntity> tagEntityQueryWrapper = new QueryWrapper<>();
        tagEntityQueryWrapper.eq("user_id", userId);
        return tagEntityDao.selectList(tagEntityQueryWrapper);
    }

    @Override
    public TagEntity getOneById(Serializable tId) {
        return tagEntityDao.selectById(tId);
    }
}

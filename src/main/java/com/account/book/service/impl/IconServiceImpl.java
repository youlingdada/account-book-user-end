package com.account.book.service.impl;

import com.account.book.dao.IconDao;
import com.account.book.entity.Icon;
import com.account.book.service.IconService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 * @author youlingdada youlingdada@163.com
 * @version 1.0
 * @createDate 2021/11/12 17:00
 */
@Service
public class IconServiceImpl implements IconService {
    @Autowired
    private IconDao iconDao;

    @Override
    public Icon selectOneById(Serializable id) {
        return iconDao.selectById(id);
    }
}

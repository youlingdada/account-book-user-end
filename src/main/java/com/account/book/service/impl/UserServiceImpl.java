package com.account.book.service.impl;

import com.account.book.dao.UserEntityDao;
import com.account.book.entity.UserEntity;
import com.account.book.service.UserService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author youlingdada youlingdada@163.com
 * @version 1.0
 * @createDate 2021/11/10 20:35
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserEntityDao userEntityDao;

    @Override
    public UserEntity getUserByUsername(String username) {
        QueryWrapper<UserEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("username", username);
        return userEntityDao.selectOne(queryWrapper);
    }

    @Override
    public UserEntity getUserByPhone(String phone) {
        QueryWrapper<UserEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("phone", phone);
        return userEntityDao.selectOne(queryWrapper);
    }

    @Override
    public UserEntity getUserById(Serializable id) {
        return userEntityDao.selectById(id);
    }

    @Override
    public UserEntity addUser(UserEntity user) {
        if (userEntityDao.insert(user) > 0) return user;
        return null;
    }

    @Override
    public UserEntity modifyUser(UserEntity user) {
        if (userEntityDao.updateById(user) > 0) return user;
        return null;
    }

    @Override
    public List<UserEntity> getUserPage(int order, int num) {
        Page<UserEntity> userEntityPage = new Page<>(order, num);
        userEntityDao.selectPage(userEntityPage, null);
        return userEntityPage.getRecords();
    }

    @Override
    public int getUserCount() {
        return userEntityDao.selectCount(null);
    }

    @Override
    public List<UserEntity> getDeletedUsers(int order, int num) {
//        计算偏移量
        int offset = order * num;
        return userEntityDao.selectDeletedUser(offset, num);
    }

    @Override
    public int getDeletedUserCount() {
        return userEntityDao.selectDeletedCount();
    }

    @Override
    public boolean deleteUsers(long[] ids) {
        List<Long> longs = new ArrayList<>();
        for (long id : ids) {
            longs.add(id);
        }
        final int i = userEntityDao.deletedUsers(longs);
        return i > 0;

    }

    @Override
    public boolean activeUsers(long[] ids) {
        List<Long> longs = new ArrayList<>();
        for (long id : ids) {
            longs.add(id);
        }
        final int i = userEntityDao.activeUsers(longs);
        return i > 0;
    }
}

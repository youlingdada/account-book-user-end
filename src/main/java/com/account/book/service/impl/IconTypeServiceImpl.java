package com.account.book.service.impl;

import com.account.book.dao.IconTypeDao;
import com.account.book.entity.IconType;
import com.account.book.service.IconTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 * @author youlingdada youlingdada@163.com
 * @version 1.0
 * @createDate 2021/11/10 20:54
 */
@Service
public class IconTypeServiceImpl implements IconTypeService {

    @Autowired
    private IconTypeDao iconTypeDao;

    @Override
    public IconType getIconTypeById(Serializable id) {
        return iconTypeDao.selectIconById(id);
    }
}

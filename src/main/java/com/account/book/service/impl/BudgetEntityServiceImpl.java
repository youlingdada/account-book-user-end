package com.account.book.service.impl;

import com.account.book.dao.BudgetEntityDao;
import com.account.book.entity.BudgetEntity;
import com.account.book.service.BudgetEntityService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author youlingdada youlingdada@163.com
 * @version 1.0
 * @createDate 2021/11/12 15:47
 */
@Service
public class BudgetEntityServiceImpl implements BudgetEntityService {
    @Autowired
    private BudgetEntityDao budgetEntityDao;



    @Override
    public BudgetEntity addBudget(BudgetEntity budgetEntity) {
        Calendar nowCalendar, budgetCalendar;
        nowCalendar = Calendar.getInstance();
        budgetCalendar = Calendar.getInstance();
        Date time = budgetEntityDao.getLastBudgetTime(budgetEntity.getUserId(), (byte) budgetEntity.getFlag());
        if (time == null) return budgetEntityDao.insert(budgetEntity) > 0 ? budgetEntity : null;
        budgetCalendar.setTime(time);
        if (nowCalendar.get(Calendar.YEAR) == budgetCalendar.get(Calendar.YEAR)) {
            if (nowCalendar.get(Calendar.MONTH) != budgetCalendar.get(Calendar.MONTH))
                return budgetEntityDao.insert(budgetEntity) > 0 ? budgetEntity : null;
        }
        if (nowCalendar.get(Calendar.YEAR) != budgetCalendar.get(Calendar.YEAR)) {
            return budgetEntityDao.insert(budgetEntity) > 0 ? budgetEntity : null;
        }
        return null;
    }

    @Override
    public BudgetEntity modifyBudgetById(BudgetEntity budgetEntity) {
        QueryWrapper<BudgetEntity> budgetEntityQueryWrapper = new QueryWrapper<>();
        budgetEntityQueryWrapper.eq("b_id", budgetEntity.getBId());
        budgetEntityQueryWrapper.eq("user_id", budgetEntity.getUserId());
        return budgetEntityDao.update(budgetEntity, budgetEntityQueryWrapper) > 0 ? budgetEntity : null;
    }

    @Override
    public boolean removeBudgetById(Serializable id, Serializable userId) {
        QueryWrapper<BudgetEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("b_id", id);
        queryWrapper.eq("user_id", userId);
        return budgetEntityDao.delete(queryWrapper) > 0;
    }

    @Override
    public List<BudgetEntity> getBudgetByUserId(Serializable userId, int order, int num) {
        Page<BudgetEntity> page = new Page<>(order, num);
        QueryWrapper<BudgetEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", userId);
        return budgetEntityDao.selectPage(page, queryWrapper).getRecords();
    }

    @Override
    public BudgetEntity getOneById(Serializable id) {
        return budgetEntityDao.selectById(id);
    }
}

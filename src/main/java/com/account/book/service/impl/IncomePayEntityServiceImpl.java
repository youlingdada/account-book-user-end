package com.account.book.service.impl;

import com.account.book.dao.IncomePayEntityDao;
import com.account.book.entity.IncomePayEntity;
import com.account.book.service.IncomePayEntityService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;

/**
 * @author youlingdada youlingdada@163.com
 * @version 1.0
 * @createDate 2021/11/12 16:59
 */
@Service
public class IncomePayEntityServiceImpl implements IncomePayEntityService {
    @Autowired
    private IncomePayEntityDao entityDao;

    @Override
    public IncomePayEntity addIncomePay(IncomePayEntity incomePayEntity) {
        return entityDao.insert(incomePayEntity) > 0 ? incomePayEntity : null;
    }

    @Override
    public IncomePayEntity modifyIncomePayById(IncomePayEntity incomePayEntity) {
        return entityDao.updateById(incomePayEntity) > 0 ? incomePayEntity : null;
    }

    @Override
    public boolean removeIncomePayById(Serializable id, Serializable userId) {
        QueryWrapper<IncomePayEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("p_id", id);
        queryWrapper.eq("user_id", userId);
        return entityDao.delete(queryWrapper) > 0;
    }

    @Override
    public IncomePayEntity getOneById(Serializable id) {
        return entityDao.selectById(id);
    }

    @Override
    public List<IncomePayEntity> getIncomePays(Serializable userId, int order, int num) {
        Page<IncomePayEntity> page = new Page<>(order, num);
        QueryWrapper<IncomePayEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", userId);
        Page<IncomePayEntity> selectPage = entityDao.selectPage(page, queryWrapper);
        return selectPage.getRecords();
    }



}

package com.account.book.service.impl;

import com.account.book.dao.BudgetDao;
import com.account.book.entity.Budget;
import com.account.book.service.BudgetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

/**
 * @author youlingdada youlingdada@163.com
 * @version 1.0
 * @createDate 2021/11/11 15:57
 */
@Service
public class BudgetServiceImpl implements BudgetService {

    @Autowired
    private BudgetDao budgetDao;

    @Override
    public Budget getBudgetById(Serializable id) {
        return budgetDao.selectById(id);
    }

    @Override
    public Budget getBudgetByUserIdMouthPay(Serializable userId, int year, int mouth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, mouth, 1, 0, 0, 0);
        Date beforeDate = calendar.getTime();
        int maximum = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        calendar.set(year, mouth, maximum, 23, 59, 59);
        Date afterDate = calendar.getTime();
        return budgetDao.selectMouthByUserId(userId, (byte) 1, beforeDate, afterDate);
    }

    @Override
    public Budget getBudgetByUserIdMouthIncome(Serializable userId, int year, int mouth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, mouth, 1, 0, 0, 0);
        Date beforeDate = calendar.getTime();
        int maximum = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        calendar.set(year, mouth, maximum, 23, 59, 59);
        Date afterDate = calendar.getTime();
        return budgetDao.selectMouthByUserId(userId, (byte) 0, beforeDate, afterDate);
    }

    @Override
    public Budget getCurrentUserIdPay(Serializable userId) {
        Calendar calendar = Calendar.getInstance();
        final int month = calendar.get(Calendar.MONTH);
        final int year = calendar.get(Calendar.YEAR);
        return getBudgetByUserIdMouthPay(userId, year, month);
    }

    @Override
    public Budget getCurrentUserIdIncome(Serializable userId) {
        Calendar calendar = Calendar.getInstance();
        final int month = calendar.get(Calendar.MONTH);
        final int year = calendar.get(Calendar.YEAR);
        return getBudgetByUserIdMouthIncome(userId, year, month);
    }


}

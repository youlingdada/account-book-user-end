package com.account.book.service.impl;

import com.account.book.dao.TagDao;
import com.account.book.entity.Tag;
import com.account.book.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;

/**
 * @author youlingdada youlingdada@163.com
 * @version 1.0
 * @createDate 2021/11/12 16:52
 */
@Service
public class TagServiceImpl implements TagService {
    @Autowired
    private TagDao tagDao;

    @Override
    public Tag getOneById(Serializable tId) {
        return tagDao.selectById(tId);
    }

    @Override
    public List<Tag> getTags(Serializable userId) {
        return tagDao.getTags(userId);
    }



}

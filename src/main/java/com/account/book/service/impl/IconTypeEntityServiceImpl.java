package com.account.book.service.impl;

import com.account.book.dao.IconTypeEntityDao;
import com.account.book.entity.IconTypeEntity;
import com.account.book.service.IconTypeEntityService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author youlingdada youlingdada@163.com
 * @version 1.0
 * @createDate 2021/11/12 16:05
 */
@Service
public class IconTypeEntityServiceImpl implements IconTypeEntityService {

    @Autowired
    private IconTypeEntityDao iconTypeEntityDao;

    @Override
    public IconTypeEntity addIconType(IconTypeEntity iconTypeEntity) {
        if (iconTypeEntityDao.insert(iconTypeEntity) > 0) return iconTypeEntity;
        return null;
    }

    @Override
    public IconTypeEntity modifyIconTypeById(IconTypeEntity iconTypeEntity) {
        if (iconTypeEntityDao.updateById(iconTypeEntity) > 0) return iconTypeEntity;
        return null;
    }

    @Override
    public List<IconTypeEntity> getIconTypeList() {
        return iconTypeEntityDao.selectList(null);
    }

    @Override
    public List<IconTypeEntity> getIconTypeList(int order, int num) {
        Page<IconTypeEntity> page = new Page<>(order, num);
        iconTypeEntityDao.selectPage(page, null);
        return page.getRecords();
    }

    @Override
    public List<IconTypeEntity> getDeletedIconTypeList(int order, int num) {
        int offset = order * num;
        return iconTypeEntityDao.selectDeletedIconTypes(offset, num);
    }

    //    带测试
    @Override
    public boolean removeIconTypeId(Serializable id) {
        return iconTypeEntityDao.deleteById(id) > 0;
    }

    @Override
    public int getIconTypeCount() {
        return iconTypeEntityDao.selectCount(null);
    }

    @Override
    public int getDeletedIconTypeCount() {
        return iconTypeEntityDao.selectDeletedIconTypesCount();
    }

    @Override
    public boolean activeIconTypes(long[] ids) {
        List<Long> longs = new ArrayList<>();
        for (long id : ids) {
            longs.add(id);
        }
        final int i = iconTypeEntityDao.activeIconTypes(longs);
        return i > 0;
    }

    @Override
    public boolean deletedIconTypes(long[] ids) {
        List<Long> longs = new ArrayList<>();
        for (long id : ids) {
            longs.add(id);
        }
        final int i = iconTypeEntityDao.deletedIconTypes(longs);
        return i > 0;
    }
}

package com.account.book.service.impl;

import com.account.book.dao.IconEntityDao;
import com.account.book.entity.IconEntity;
import com.account.book.service.IconEntityService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author youlingdada youlingdada@163.com
 * @version 1.0
 * @createDate 2021/11/12 16:58
 */
@Service
public class IconEntityServiceImpl implements IconEntityService {

    @Autowired
    private IconEntityDao iconEntityDao;

    @Override
    public IconEntity addIcon(IconEntity icon) {
        return iconEntityDao.insert(icon) > 0 ? icon : null;
    }

    @Override
    public IconEntity getOneById(Serializable iId) {
        return iconEntityDao.selectById(iId);
    }


    @Override
    public IconEntity modifyIconById(IconEntity icon) {
        return iconEntityDao.updateById(icon) > 0 ? icon : null;
    }

    @Override
    public boolean removeIconById(Serializable id, Serializable userId) {
        QueryWrapper<IconEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("i_id", id);
        queryWrapper.eq("user_id", userId);
        return iconEntityDao.delete(queryWrapper) > 0;
    }

    @Override
    public List<IconEntity> getIcons(int order, int num) {
        Page<IconEntity> page = new Page<>(order, num);
        return iconEntityDao.selectPage(page, null).getRecords();
    }

    public List<IconEntity> getDeletedIcons(int order, int num) {
        return iconEntityDao.selectDeletedIcon(order, num);
    }

    @Override
    public int getIconCount() {
        return iconEntityDao.selectCount(null);
    }

    @Override
    public int getDeletedIconsCount() {
        return iconEntityDao.selectDeletedIconCount();
    }

    @Override
    public boolean activeIcons(long[] ids) {
        List<Long> longs = new ArrayList<>();
        for (long id : ids) {
            longs.add(id);
        }
        final int i = iconEntityDao.activeIcons(longs);
        return i > 0;
    }

    @Override
    public boolean deletedIcons(long[] ids) {
        List<Long> longs = new ArrayList<>();
        for (long id : ids) {
            longs.add(id);
        }
        final int i = iconEntityDao.deletedIcons(longs);
        return i > 0;
    }
}

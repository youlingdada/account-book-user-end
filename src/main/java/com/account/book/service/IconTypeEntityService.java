package com.account.book.service;


import com.account.book.entity.IconTypeEntity;

import java.io.Serializable;
import java.util.List;

/**
 * @author youlingdada youlingdada@163.com
 * @version 1.0
 * @createDate 2021/11/12 16:04
 */
public interface IconTypeEntityService {
    //    添加一个IconType
    IconTypeEntity addIconType(IconTypeEntity iconTypeEntity);

    //    按照id更新一个IconType
    IconTypeEntity modifyIconTypeById(IconTypeEntity iconTypeEntity);

    //    获取全部的IconType
    List<IconTypeEntity> getIconTypeList();

    // 按照id删除
    boolean removeIconTypeId(Serializable id);

    // 分页查询
    List<IconTypeEntity> getIconTypeList(int order, int num);

    // 分页查询逻辑删除的类型
    List<IconTypeEntity> getDeletedIconTypeList(int order, int num);


    // 获取正常类型数量
    int getIconTypeCount();

    // 获取逻辑删除的类型数量
    int getDeletedIconTypeCount();

    // 批量激活
    boolean activeIconTypes(long[] ids);

    // 批量删除
    boolean deletedIconTypes(long[] ids);
}

package com.account.book.service;

import com.account.book.entity.Tag;

import java.io.Serializable;
import java.util.List;

/**
 * @author youlingdada youlingdada@163.com
 * @version 1.0
 * @createDate 2021/11/12 16:52
 */
public interface TagService {
    //    4表联查 慎用
    Tag getOneById(Serializable tId);

    List<Tag> getTags(Serializable userId);

}

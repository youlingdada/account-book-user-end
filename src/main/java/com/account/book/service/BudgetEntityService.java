package com.account.book.service;

import com.account.book.entity.BudgetEntity;

import java.io.Serializable;
import java.util.List;

/**
 * @author youlingdada youlingdada@163.com
 * @version 1.0
 * @createDate 2021/11/11 16:00
 */
public interface BudgetEntityService {
    //    添加一条用户预算记录
    BudgetEntity addBudget(BudgetEntity budgetEntity);

    //    修改一条用户的记录
    BudgetEntity modifyBudgetById(BudgetEntity budgetEntity);

    //    删除一条预算记录
    boolean removeBudgetById(Serializable id,Serializable userId);

    //    获取用户的所有预算
    List<BudgetEntity> getBudgetByUserId(Serializable id,int order,int num);

    //    获取当月的预算

    BudgetEntity getOneById(Serializable id);
}

package com.account.book.service;

import com.account.book.entity.TagEntity;

import java.io.Serializable;
import java.util.List;

/**
 * @author youlingdada youlingdada@163.com
 * @version 1.0
 * @createDate 2021/11/12 16:34
 */
public interface TagEntityService {
    TagEntity addTagEntity(TagEntity tagEntity);

    TagEntity updateTagEntity(TagEntity tagEntity);

    boolean removeTagEntityById(Serializable tId);

    List<TagEntity> getTagEntityByUserId(Serializable userId);

    TagEntity getOneById(Serializable tId);
}

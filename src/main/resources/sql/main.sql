/*
Navicat MySQL Data Transfer

Source Server         : 本地数据库
Source Server Version : 80022
Source Host           : localhost:3306
Source Database       : account_book

Target Server Type    : MYSQL
Target Server Version : 80022
File Encoding         : 65001

Date: 2021-10-27 19:30:33
*/

SET
FOREIGN_KEY_CHECKS=0;

DROP
DATABASE IF EXISTS `account_book`;
CREATE
DATABASE `account_book` CHARACTER SET utf8 COLLATE utf8_general_ci;

USE
account_book;

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`
(
    `u_id`        bigint         NOT NULL,
    `phone`       varchar(11)    NOT NULL,
    `username`    varchar(20)    NOT NULL,
    `pay`         decimal(10, 2) NOT NULL,
    `income`      decimal(10, 2) NOT NULL,
    `money`       decimal(10, 2) NOT NULL DEFAULT '0.00',
    `deleted`     int            NOT NULL DEFAULT '0',
    `version`     int            NOT NULL DEFAULT '0',
    `create_time` datetime       NOT NULL,
    `update_time` datetime       NOT NULL,
    PRIMARY KEY (`u_id`),
    UNIQUE KEY `username` (`username`),
    UNIQUE KEY `phone` (`phone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user`
VALUES ('1456905767577747458', '15284052715', 'lingdada', '0.00', '0.00', '0.00', '0', '0', '2021-11-06 08:46:18',
        '2021-11-06 12:50:41');
INSERT INTO `user`
VALUES ('1456958831051460609', '15284052714', 'youlingdada', '0.00', '0.00', '0.00', '0', '0', '2021-11-06 12:17:09',
        '2021-11-06 12:17:09');


-- ----------------------------
-- Table structure for `budget`
-- ----------------------------
DROP TABLE IF EXISTS `budget`;
CREATE TABLE `budget`
(
    `b_id`          bigint         NOT NULL,
    `money`         decimal(10, 2) NOT NULL DEFAULT '0.00',
    `current_money` decimal(10, 2) NOT NULL DEFAULT '0.00',
    `award`         varchar(100)            DEFAULT NULL,
    `user_id`       bigint         NOT NULL,
    `flag`          tinyint        NOT NULL,
    `version`       int            NOT NULL DEFAULT '0',
    `create_time`   datetime       NOT NULL,
    `update_time`   datetime       NOT NULL,
    PRIMARY KEY (`b_id`),
    KEY             `user_id` (`user_id`),
    CONSTRAINT `budget_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`u_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of budget
-- ----------------------------
INSERT INTO `budget`
VALUES ('1456959659548205058', '0.00', '0.00', 'hello world', '1456905767577747458', '1', '1', '2021-11-06 12:20:26',
        '2021-11-06 12:20:26');


-- ----------------------------
-- Table structure for `icon_type`
-- ----------------------------
DROP TABLE IF EXISTS `icon_type`;
CREATE TABLE `icon_type`
(
    `t_id`        bigint      NOT NULL,
    `name`        varchar(10) NOT NULL,
    `deleted`     int         NOT NULL DEFAULT '0',
    `version`     int         NOT NULL DEFAULT '0',
    `create_time` datetime    NOT NULL,
    `update_time` datetime    NOT NULL,
    PRIMARY KEY (`t_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of icon_type
-- ----------------------------
INSERT INTO `icon_type`
VALUES ('1456971922954809346', '娱乐', '0', '0', '2021-11-06 13:09:10', '2021-11-06 13:09:10');


-- ----------------------------
-- Table structure for `icon`
-- ----------------------------
DROP TABLE IF EXISTS `icon`;
CREATE TABLE `icon`
(
    `i_id`         bigint                                                  NOT NULL,
    `url`          varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
    `icon_type_id` bigint                                                  NOT NULL,
    `deleted`      int                                                     NOT NULL DEFAULT '0',
    `version`      int                                                     NOT NULL DEFAULT '0',
    `create_time`  datetime                                                NOT NULL,
    `update_time`  datetime                                                NOT NULL,
    PRIMARY KEY (`i_id`),
    KEY            `icon_type_id` (`icon_type_id`),
    CONSTRAINT `icon_ibfk_1` FOREIGN KEY (`icon_type_id`) REFERENCES `icon_type` (`t_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of icon
-- ----------------------------
INSERT INTO `icon`
VALUES ('1457171882690793474', 'test url', '1456971922954809346', '0', '0', '2021-11-07 02:23:44',
        '2021-11-07 02:23:44');
INSERT INTO `icon`
VALUES ('1457171889028386818', 'test url2', '1456971922954809346', '0', '0', '2021-11-07 02:23:46',
        '2021-11-07 02:23:46');
INSERT INTO `icon`
VALUES ('1457335170401595394', 'test url3', '1456971922954809346', '0', '0', '2021-11-07 13:12:35',
        '2021-11-07 13:12:35');
INSERT INTO `icon`
VALUES ('1457335177175396353', 'test url4', '1456971922954809346', '0', '0', '2021-11-07 13:12:37',
        '2021-11-07 13:12:37');

-- ----------------------------
-- Table structure for `tag`
-- ----------------------------
DROP TABLE IF EXISTS `tag`;
CREATE TABLE `tag`
(
    `g_id`        bigint      NOT NULL,
    `name`        varchar(10) NOT NULL,
    `icon_id`     bigint      NOT NULL,
    `user_id`     bigint      NOT NULL,
    `version`     int         NOT NULL DEFAULT '0',
    `create_time` datetime    NOT NULL,
    `update_time` datetime    NOT NULL,
    PRIMARY KEY (`g_id`),
    KEY           `icon_id` (`icon_id`),
    CONSTRAINT `tag_ibfk_1` FOREIGN KEY (`icon_id`) REFERENCES `icon` (`i_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
    CONSTRAINT `tag_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`u_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tag
-- ----------------------------

-- ----------------------------
-- Table structure for `income_pay`
-- ----------------------------
DROP TABLE IF EXISTS `income_pay`;
CREATE TABLE `income_pay`
(
    `p_id`        bigint                                                  NOT NULL,
    `tag_id`      bigint                                                  NOT NULL,
    `description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
    `money`       decimal(10, 2)                                          NOT NULL DEFAULT '0.00',
    `user_id`     bigint                                                  NOT NULL,
    `flag`        tinyint                                                 NOT NULL,
    `version`     int                                                     NOT NULL DEFAULT '0',
    `create_time` datetime                                                NOT NULL,
    `update_time` datetime                                                NOT NULL,
    PRIMARY KEY (`p_id`),
    KEY           `tag_id` (`tag_id`),
    KEY           `user_id` (`user_id`),
    CONSTRAINT `income_pay_ibfk_1` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`g_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
    CONSTRAINT `income_pay_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`u_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of income_pay
-- ----------------------------